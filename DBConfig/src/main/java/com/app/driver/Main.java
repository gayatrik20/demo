package com.app.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.app.EmployeeDAO;

public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
			    EmployeeDAO empDAO = (EmployeeDAO)ctx.getBean("empDAO");
			    System.out.println(empDAO);
	}

}
