package com.cts.proj.model;

public class Employee {



private int employeeId;
private String empName;
private String cohortCode;
public int getEmployeeId() {
return employeeId;
}
public void setEmployeeId(int employeeId) {
this.employeeId = employeeId;
}
public String getEmpName() {
return empName;
}
public void setEmpName(String empName) {
this.empName = empName;
}
public String getCohortCode() {
return cohortCode;
}
public void setCohortCode(String cohortCode) {
this.cohortCode = cohortCode;
}
public void display() {
System.out.println(employeeId);
System.out.println(empName);
System.out.println(cohortCode);
}}
