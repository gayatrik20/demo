package com.cts.project;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext ctx = new ClassPathXmlApplicationContext("pSchema.xml");
    	Student student = (Student) ctx.getBean("student");
    	System.out.println(student);
    }
}
